import Swiper from 'swiper'

const specialistSlider = {
    init() {
        this.$wrapper = $('.js-specialists-slider')
        this.$slider = this.$wrapper.find('.swiper-container')

        this.initSlider()

    },

    initSlider() {
        new Swiper(this.$slider, {
            slidesPerView: 6,
            spaceBetween: 10,
            navigation: {
                nextEl: this.$wrapper.find('.swiper-button-next'),
                prevEl: this.$wrapper.find('.swiper-button-prev')
            },
            pagination: {
                el: this.$wrapper.find('.swiper-pagination'),
                type: 'fraction'
            }
        })
    }
}



export {specialistSlider}