import {modalHandler} from './handlers/modals'
import {specialistSlider} from './handlers/sliders'

function initApp() {
    window.$body = $('.js-body')

    modalHandler.init()

    specialistSlider.init()
}

export {initApp}